package cz.vse.java.xvalm00.adventuracv.main;

import cz.vse.java.xvalm00.adventuracv.gui.HerniPlocha;
import cz.vse.java.xvalm00.adventuracv.gui.PanelBatohu;
import cz.vse.java.xvalm00.adventuracv.gui.PanelVychodu;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class AdventuraZaklad extends Application {

    private IHra hra = Hra.getSingleton();
    private TextField prikazovePole;
    private Label zadejPrikazLabel;
    private HBox spodniBox;
    private TextArea konzole;
    private BorderPane hraBorderPane = new BorderPane();
    private VBox menuAHraVBox = new VBox();
    private MenuBar menuBar;
    private HerniPlocha mapaHry;
    private PanelVychodu panelVychodu;
    private PanelBatohu panelBatohu;

    public static void main(String[] args) {
        if (args.length == 0) {
            launch(args);
        } else {
            if (args[0].equals("-text")) {
                IHra hra = Hra.getSingleton();
                TextoveRozhrani ui = new TextoveRozhrani(hra);
                ui.hraj();
                System.exit(0);
            } else if (args[0].equals("-gui")) {
                launch(args);
            }
            else {
                System.out.println("Byl zadan neplatny parametr.");
            }
        }
    }

    @Override
    public void start(Stage primaryStage) {
        mapaHry = new HerniPlocha(hra.getHerniPlan());
        AnchorPane mapaHryAnchorPane = mapaHry.getAnchorPane();

        panelVychodu = new PanelVychodu(hra.getHerniPlan());
        ListView<String> listView = panelVychodu.getListView();

        panelBatohu = new PanelBatohu(hra);

        pripravKonzoli();
        pripravPrikazovePole();
        pripravSpodniPanel();
        pripravHraBorderPane(mapaHryAnchorPane, listView, panelBatohu);
        pripravMenu();

        menuAHraVBox.getChildren().addAll(menuBar, hraBorderPane);
        pripravScenuAStage(primaryStage);
        prikazovePole.requestFocus();
    }


    private void pripravMenu() {
        Menu souborMenu = new Menu("Soubor");

        // nova hra
        ImageView novaHraIkonka = new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream("/zdroje/new.gif")));
        MenuItem novaHra = new MenuItem("Nova hra", novaHraIkonka);
        novaHra.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));
        novaHra.setOnAction(event -> {
            hra = Hra.restartHry();
            konzole.setText(hra.vratUvitani());
            hra.getHerniPlan().registerObserver(mapaHry);
            hra.getHerniPlan().registerObserver(panelVychodu);
            hra.getBatoh().registerObserver(panelBatohu);
            mapaHry.novaHra(hra.getHerniPlan());
            panelVychodu.novaHra(hra.getHerniPlan());
            panelBatohu.novaHra(hra.getBatoh());
            prikazovePole.requestFocus();
        });

        SeparatorMenuItem separator = new SeparatorMenuItem();

        MenuItem konec = new MenuItem("Konec");
        konec.setOnAction(event -> System.exit(0));

        souborMenu.getItems().addAll(novaHra, separator, konec);

        Menu napovedaMenu = new Menu("Nápověda");
        MenuItem oAplikaci = new MenuItem("O aplikaci");
        MenuItem napovedaKAplikaci = new MenuItem("Nápověda k aplikaci");
        napovedaMenu.getItems().addAll(oAplikaci, napovedaKAplikaci);

        oAplikaci.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Graficka adventura");
            alert.setHeaderText("JavaFX adventura");
            alert.setContentText("verze LS 2020");
            alert.showAndWait();
        });

        napovedaKAplikaci.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Napoveda k aplikaci");
            WebView webView = new WebView();
            webView.getEngine().load(AdventuraZaklad.class.getResource("/zdroje/napoveda.html").toExternalForm());
            stage.setScene(new Scene(webView, 600, 600));
            stage.show();
        });

        menuBar = new MenuBar();

        menuBar.getMenus().addAll(souborMenu, napovedaMenu);
    }

    private void pripravScenuAStage(Stage primaryStage) {
        Scene scene = new Scene(menuAHraVBox, 600, 450);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Adventura");
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });
    }

    private void pripravHraBorderPane(AnchorPane planekHry, ListView<String> listView, PanelBatohu panelBatohu) {
        hraBorderPane.setTop(planekHry);
        hraBorderPane.setRight(listView);
        hraBorderPane.setCenter(konzole);
        hraBorderPane.setLeft(panelBatohu.getPanel());
        hraBorderPane.setBottom(spodniBox);
    }

    private void pripravSpodniPanel() {
        spodniBox = new HBox();
        spodniBox.setAlignment(Pos.CENTER);
        spodniBox.getChildren().addAll(zadejPrikazLabel, prikazovePole);
    }

    private void pripravPrikazovePole() {
        prikazovePole = new TextField();
        prikazovePole.setOnAction(event -> {
            String prikaz = prikazovePole.getText();
            konzole.appendText("\n" + prikaz + "\n");
            prikazovePole.setText("");
            String odpovedHry = hra.zpracujPrikaz(prikaz);
            konzole.appendText("\n" + odpovedHry + "\n");

            if (hra.konecHry()) {
                prikazovePole.setEditable(false);
            }
        });

        zadejPrikazLabel = new Label("Zadej příkaz: ");
        zadejPrikazLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
    }

    private void pripravKonzoli() {
        konzole = new TextArea();
        konzole.setText(hra.vratUvitani());
        konzole.setEditable(false);
    }
}
